const User = require("../models/User")
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.checkEmailExists = (reqBody) => {
	return User.find({email : reqBody.email}).then(result => {
		return (result.length > 0)
	})
}

module.exports.registerUser = (reqBody => {
	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		mobileNo : reqBody.mobileNo,
		password : reqBody.password
	})

	return newUser.save().then((user, error) => {
		return !error;
	})
})

module.exports.loginUser = (reqBody) => {
	return User.findOne({email : reqBody.email}).then(result => {
		if(result == null){
			return false;
		}
		else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password,result.password);
			if (isPasswordCorrect){
				return { access : auth.createAccessToken(result)}
			}
			else{
				return false;
			}
		}
	})
}

module.exports.getProfeile = (reqBody) => {
	return User.findById(reqBody.id).then(result => {
		if (result == null){
			return false;
		}
		else {
			profile = result;
			profile.password = "";
			return profile.;
		}
	})
}